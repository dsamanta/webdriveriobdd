exports.module = { 
    DEV:"https://the-internet.herokuapp.com/",
    TEST:"https://www.globalsqa.com/angularJs-protractor/BankingProject/#/login",
    STAGING:"https://testpages.herokuapp.com/styled/index.html",
    PROD:"http://uitestingplayground.com/"
}