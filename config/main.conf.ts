import envData from '../test/testData/envData.json'

let appbaseURL: string;
console.log(process.env.ENV);

if (process.env.ENV === 'DEV') {
    appbaseURL = envData.DEV.baseUrl;
} else if (process.env.ENV === 'TEST') {
    appbaseURL = envData.TEST.baseUrl;
}else if (process.env.ENV === 'STAGING') {
    appbaseURL = envData.STAGING.baseUrl;
}else if (process.env.ENV === 'PROD') {
    appbaseURL = envData.PROD.baseUrl;
} else {
    console.log("Please enter Valid Environment !!")
    process.exit()
}


exports.config = { 
    autoCompileOpts: {
        autoCompile: true,
        tsNodeOpts: {
            transpileOnly: true,
            project: 'test/tsconfig.json'
        }
    },
    specs: [
        './features/**/*.feature'
    ],
    exclude: [
        // 'path/to/excluded/files'
    ],
    maxInstances: 10,
    logLevel: 'info',
    bail: 0,
    //baseUrl: 'https://the-internet.herokuapp.com/',
    baseUrl: appbaseURL,
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    services: ['chromedriver'],
    framework: 'cucumber',
    reporters: [
        'spec',
        
        ['json', {
            outputDir: './test/reports/json-results'
            }],
    
          ['junit', {
            outputDir: './test/reports/junit-results',
            outputFileFormat: function(options) {
                  return `results-${options.cid}.${options.capabilities}.xml`
              }
          }],
    ],

    cucumberOpts: {
        require: ['./features/step-definitions/steps.ts'],
        backtrace: false,
        requireModule: [],
        dryRun: false,
        failFast: false,
        snippets: true,
        source: true,
        strict: false,
        tagExpression: '',
        timeout: 60000,
        ignoreUndefinedDefinitions: false
    },
}
