const {config} = require('./main.conf')

exports.config = { 
    ...config,
    ...{
    capabilities: [
         {
        maxInstances: 1,
        browserName: 'chrome',
        acceptInsecureCerts: true
    }
],
    }
}