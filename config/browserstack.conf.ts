import envData from '../test/testData/envData.json'

let appbaseURL: string;
console.log(process.env.ENV);

if (process.env.ENV === 'DEV') {
    appbaseURL = envData.DEV.baseUrl;
} else if (process.env.ENV === 'TEST') {
    appbaseURL = envData.TEST.baseUrl;
}else if (process.env.ENV === 'STAGING') {
    appbaseURL = envData.STAGING.baseUrl;
}else if (process.env.ENV === 'PROD') {
    appbaseURL = envData.PROD.baseUrl;
} else {
    console.log("Please enter Valid Environment !!")
    process.exit()
}

exports.config = {
    user: 'krittikasamanta_q59eTg',
    key: '7CFYX4jPDmZqfsepSHWK',  // Your BrowserStack credentials go here
  
    specs: [
        './features/**/*.feature'
    ],
    exclude: [],
  
    capabilities: [{
      browserName: 'Chrome',  // Signifies on what platform your test will run. You can define other capabilities here.
      name: 'single_test',
      build: 'first-webdriverio-browserstack-build'  // The name of test and name of build is being defined here
    }],
  
    logLevel: 'warn',
    coloredLogs: true,
    screenshotPath: './errorShots/',
    baseUrl: appbaseURL,
    waitforTimeout: 30000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    framework: 'cucumber',
    host: 'hub.browserstack.com',  // This line is important for your tests to run on BrowserStack
    cucumberOpts: {
        require: ['./features/step-definitions/steps.ts'],
        backtrace: false,
        requireModule: [],
        dryRun: false,
        failFast: false,
        snippets: true,
        source: true,
        strict: false,
        tagExpression: '',
        timeout: 60000,
        ignoreUndefinedDefinitions: false
    },
    // before: function () {
    //   var chai = require('chai');
    //   global.expect = chai.expect;
    //   chai.Should();
    // },
    // framework: 'mocha',
    // mochaOpts: {
    //   ui: 'bdd',
    //   timeout: 60000
    // },
  
    // The afterTest function is used to mark the test status on BrowserStack using JavaScript executor based on the assertion status of your tests
    afterTest: function (test, context, { error, result, duration, passed, retries }) {
      if(passed) {
        browser.executeScript('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed","reason": ""}}');
      } else {
        browser.executeScript('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed","reason": ""}}');
      }
    }
  }
  