import Page from '../pages/page'

class Abpage extends Page {

    get abtestHeader () { return $("h3") }
   

    async open (path) {
        await super.open(path)
    }

}

export default new Abpage()