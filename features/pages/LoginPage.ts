import Page from '../pages/page'

class LoginPage extends Page {

    get username () { return $('#username') }
    get password () { return $('#password') }
    get submitBtn () { return $('form button[type="submit"]') }
    get flash () { return $('#flash') }
    get headerLinks () { return $$('#header a') }

    async open (path) {
        await super.open(path)
    }

    async submit () {
        await this.submitBtn.click()
    }

}

export default new LoginPage()